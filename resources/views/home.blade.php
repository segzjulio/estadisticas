@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
        <title>replit</title>
        <link href="https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css" rel="stylesheet"/>

    </head>
    <body>
       

        <div id="app">
            <formulario></formulario>
        </div>


        <script src="{{ mix('js/app.js') }}"></script>
        {{-- <script src="./script.js"></script> --}}
    </body>
</html>
@endsection


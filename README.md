# Estadisticas



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## 🏁 Instalación local
1. Clonar repositorio en laragon (ruta=> C:/laragon/wwww/).
2. Crear un archivo .env copiando lo del .env.example
3. una vez ubicado en la carpeta del proyecto ejecutar los siguientes comandos:
    2.1. composer install
    2.2. npm install
    2.3. php artisan migrate
4. Una vez iniciado el proyecto, la primera vista sera el de iniciar sesion.
5. se debe dirigir al apartado para crear una cuenta (register)
6. una vez logeado se vera la pantalla principal, con el grafico vacio
7. en este punto ya podra agregar datos en el formulario y viendo en el grafico.
8. Se agrego una data table en el cual se puede apreciar un CRUD.


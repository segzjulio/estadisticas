<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Datos extends Model
{
    protected $table = 'datos';

    protected $fillable = [
        'user_id',
        'fecha',
        'monto',
    ];

    protected $casts = [
        'created_at' => 'datetime:d-m-Y H:i:s',
    ];
}

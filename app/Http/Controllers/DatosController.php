<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Datos;

class DatosController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request){
        // dd($request->date + 1);
        $user_id = \Auth::id();

        $request->validate([
            "date" => "required|integer",
            "monto" => "required|max:100000000|integer",
        ], [
            'date.required' => 'Fecha es requerida',
            'monto.required' => 'El monto es requerido',
        ]);

        $datos = new Datos();
        $datos->fecha = $request->date;
        $datos->monto = $request->monto;
        $datos->user_id = $user_id;
        $datos->save();

        $getDatos = Datos::select('id','fecha as date', 'monto as value')->where('user_id', $user_id)->orderBy('fecha', 'desc')->get();
        return response()->json([
            'registro' => $datos->id,
            'status' => 'Ok',
            'code' => 200,
            'datos' => $getDatos,
        ]);
    }

    public function getDatos(){

        $user_id = \Auth::id();

        $datos = Datos::select('id','fecha as date', 'monto as value')->where('user_id', $user_id)->orderBy('fecha', 'desc')->get();
        // dd($datos);
        return response()->json([
            'datos' => $datos
        ]);
    }

    public function editar(Request $request){

        $user_id = \Auth::id();
        $registro = Datos::where('id', $request->id)->first();

        $registro->monto = $request->monto;
        $registro->fecha = $request->date;

        $registro->update();
        $datos = Datos::select('id','fecha as date', 'monto as value')->where('user_id', $user_id)->orderBy('fecha', 'desc')->get();
        return response()->json([
            'message' => 'Registro Updateado correctamente',
            'registro' => $registro->id,
            'datos' => $datos,
        ]);
    }

    public function delete(Request $request){

        $user_id = \Auth::id();
        $registro = Datos::where('id', $request->id)->first();

        $registro->delete();
        $datos = Datos::select('id','fecha as date', 'monto as value')->where('user_id', $user_id)->orderBy('fecha', 'desc')->get();
        return response()->json([
            'status' => 'OK',
            'message' => 'Eliminado correctamente',
            'datos' => $datos,
        ]);
    }
}

<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DatosController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::post('/guardar', [DatosController::class, 'store'])->name('store');
Route::get('/getDatos', [DatosController::class, 'getDatos'])->name('getDatos');
Route::post('/editar', [DatosController::class, 'editar'])->name('editar');
Route::post('/delete', [DatosController::class, 'delete'])->name('delete');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
